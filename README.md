# Ansible Collection - ec.rps_nginx

## Installation

Add to your `requirements.yml`:

```yaml
collections:
  - name: https://code.europa.eu/digit-c4/sys-ansible-collection.git
    type: git
    version: v0.0.1
```

## Documentation

For more information:

 - the [CONTRIBUTING](./CONTRIBUTING.md) document describes how to contribute to the repository
 - consult the [documentation](https://digit-c4.pages.code.europa.eu/rps/nginx-ansible-collection)


