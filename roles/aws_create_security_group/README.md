aws_create_security_group
=========

This Role creates a security group which is connected to the vpc.

Important Info
--------------

This Role is normally used in a Workflow to create an AWS bubble.

Role Variables
--------------

    access_key:                 AWS access_key
    secret_key:                 AWS secret_key
    aws_region:                 AWS region
    vpc_id:                     AWS VPC id ( bubble id )
    security_group_rules:
      - name:                   Name of the Security group
        rules:
          - proto: tcp          Example #1
            from_port: 80
            to_port: 80
            cidr_ip: 0.0.0.0/0

          - proto: tcp          Example #2
            from_port: 443
            to_port: 443
            cidr_ip: 0.0.0.0/0
