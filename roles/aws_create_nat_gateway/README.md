aws_create_nat_gateway
=========

This role connects a subnet to an nat Gateway in AWS.
A NAT Gateway enables instances in a private subnet to connect to the internet, but prevents the internet from connecting with those instances.

Important Info
--------------

This role was part of a workflow but not needed anymore for the AWS bubble creation.

Role Variables
--------------

    access_key:  AWS access_key
    secret_key:  AWS secret_key
    aws_region:  AWS region
    vpc_id:      AWS VPC id ( bubble id )
    subnet_id:   AWS subnet id
    nat_id:      AWS Nat id
    nat_gateway: Nat gateway name
