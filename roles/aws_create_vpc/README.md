aws_create_vpc
=========

This Role creates a vpc ( bubble ).

Important Info
--------------

This Role is normally used in a Workflow to create an AWS bubble.

Role Variables
--------------

    access_key:  AWS access_key
    secret_key:  AWS secret_key
    aws_region:  AWS region
    vpcs:
      - vpc_name: VPC name
        ip_range: Subnet for example 192.168.0.0/27
