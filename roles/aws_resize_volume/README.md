aws_resize_volume
=========

This Role changes the volume size of a vm in AWS.

Important Info
--------------

After using this role restart the VM.

Role Variables
--------------

    access_key:  AWS access_key
    secret_key:  AWS secret_key
    aws_region:  AWS region
    instance_id: AWS VM id
