aws_create_subnet
=========

This Role creates a subnet which is connected to the vpc.

Important Info
--------------

This Role is normally used in a Workflow to create an AWS bubble.

Role Variables
--------------

    access_key:  AWS access_key
    secret_key:  AWS secret_key
    aws_region:  AWS region
    vpc_id:      AWS VPC id ( bubble id )
    subnets:
      - name:    Subnet name                 
        cidr:    Subnet for example 192.168.0.0/27
