aws_vpc_add_transit_gateway
=========

This Role add the transit gateway

Important Info
--------------

This Role is normally used in a Workflow to create an AWS bubble.

Role Variables
--------------

    access_key:         AWS access_key
    secret_key:         AWS secret_key
    aws_region:         AWS region
    vpc_id:             AWS VPC id ( bubble id )
    transit_gateway:    AWS transit gateway id
    tg_name:            Transit gateway name
    subnet_id:          AWS Subnet id
