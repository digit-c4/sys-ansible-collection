#!/bin/bash

_verbose(){
    # This function should be a simple debug function, which will print the line given based on debug level
    # implement in getops the following line:
    # (( DEBUG_LEVEL++))
    local LEVEL=1 c printout
    local funcnamenumber
    (( funcnamenumber=${#FUNCNAME[@]} - 2 ))
    : "${DEBUG_LEVEL:=0}"
    (( DEBUG_LEVEL == 0 )) && return
    # Add level 1 if first char is not set a number
    [[ "$1" =~ ^[0-9]$ ]] && { LEVEL=$1; shift; }

    (( LEVEL <= DEBUG_LEVEL )) && {
        until (( ${#c} == LEVEL )); do c+=":"; done
        if (( funcnamenumber > 0 )); then
            printout+="("
            for ((i=1;i<=funcnamenumber;i++)); do
                printout+="${FUNCNAME[$i]} <- "
            done
            printout="${printout% <- }) - "
        fi
        printf '%-7s %s %s\n' "+ $c" "$printout" "$*" 1>&2
    }
}

stack(){
    local err=$?

    set +o xtrace

    printf 'Error in %s:%s. %s exited with status %s\n' "${BASH_SOURCE[1]}" "${BASH_LINENO[0]}" "${BASH_COMMAND}" "$err"
    # Print out the stack trace described by $function_stack  

    if (( ${#FUNCNAME[@]} >> 1 )); then
        printf "Call tree:\n"
        for ((i=1;i<${#FUNCNAME[@]}-1;i++)); do
            printf ' : %s:%s %s(...)\n' "${BASH_SOURCE[$i+1]}" "${BASH_LINENO[$i]}" "${FUNCNAME[$i]}"
        done
    fi
}

info(){ printf '%(%Y-%m-%d %H:%M:%S)T id=%s %s\n' -1 "$_id" "$*"; }
err(){ printf '%(%Y-%m-%d %H:%M:%S)T id=%s %s\n' -1 "$_id" "$*"; exit 1; }
main(){

    set -o pipefail
    set -o errtrace
    trap 'stack' ERR

    programname=${0##*/}

    while getopts ":vh" opt; do
        case $opt in
            v)
                (( DEBUG_LEVEL++ ))
                ;;
            h)
                info "Usage: $0 <config_file> <interface>"
                info "Example: $0 /etc/snet/$programname/config.sh eth0"
                info "Configuration file example:"
                info "target_net=ovpnsrv-04_OpenVPN"
                info "target_routes=(10.0.0.0/24 10.1.0.0/24)"
                exit 0
                ;;
            \?)
                err "Invalid option: -$OPTARG"
                ;;
        esac
    done

    _id="$RANDOM"
    exec &> >(logger -s -t "$programname")

    # Source configuration file
    [[ -f "$1" ]] || {
        info "No configuration file found! $1"
        exit 0
    }
    [[ -z "$2" ]] && {
        info "Missing mandatory argument!"
        info "Usage: $0 <config_file> <interface>"
        info "Example: $0 /etc/snet/$programname/config.sh eth0"
        info "Configuration file example:"
        info "target_net=ovpnsrv-04_OpenVPN"
        info "target_routes=(10.0.0.0/24 10.1.0.0/24)"
        exit 1
    }

    _verbose 1 "Source configuration file $1"
    # shellcheck disable=SC1090
    source "$1"

    for var in target_net target_routes; do
        [[ -z "${!var}" ]] && {
            err "Variable $var is not set in configuration file $1"
        }
    done

    info "Checking if routes are needed for $2"

    brip="$(ip addr show dev "$2" scope global | grep -oP '(?<=inet\s)\d+(\.\d+){3}')"
    _verbose 1 "Bridge IP: $brip for $2"

    # shellcheck disable=SC2154
    read -r dockerid _ <<< "$(docker network list | grep "${target_net}")"
    _verbose 1 "Docker network ID: $dockerid"

    dockerip="$(docker inspect "${dockerid}" | grep "Gateway"| grep -Po '\d+(\.\d+){3}')"
    _verbose 1 "Docker network Gateway: $dockerip"
    if [[ "$brip" == "$dockerip" ]]; then
        # Triggered for the target net
        info "Adding ovpn client routes to ${brip} for net ${target_net}"
        if ! ip link set "$2" up; then
            err "ip link set $2 up : failed"
        fi
        info "ip link set $2 up : sucessfully"

        # shellcheck disable=SC2154
        for r in "${target_routes[@]}"; do
            if ! ip route add "${r}" via "${brip}" dev "$2" table internal; then
                err "ip route add ${r} via ${brip} dev $2 table internal : failed"
            fi
            info "ip route add ${r} via ${brip} dev $2 table internal : sucessfully"
        done
    fi

    info "Adding routes done"
}

main "$@"
