aws_create_routing_table
=========

This Role creates a routing table which is connected to the vpc and the subnet.

Important Info
--------------

This Role is normally used in a Workflow to create an AWS bubble.

Role Variables
--------------

    access_key:      AWS access_key
    secret_key:      AWS secret_key
    aws_region:      AWS region
    vpc_id:          AWS VPC id ( bubble id )
    subnet_id:       AWS subnet id
    nat_id:          AWS Nat id
    nat_gateway:     Nat gateway name
    tags_list:
      - key: Name
        value:       Routing table name
    igw_id:          Internet Gateway id   
    transit_gateway: Transit gateway id
