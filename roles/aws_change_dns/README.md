aws_change_dns
=========

This role changes the dns for the VPC ( Bubble ).

Role Variables
--------------

    access_key: AWS access_key
    secret_key: AWS secret_key
    aws_region: AWS region
    vpc_id:     AWS VPC id ( bubble id )
    dns_server: DNS Server
