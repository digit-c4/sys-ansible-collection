aws_create_internet_gateway
=========

This role creates an internet Gateway in AWS.

Important Info
--------------

This Role is normally used in a Workflow to create an AWS bubble

Role Variables
--------------

    access_key: AWS access_key
    secret_key: AWS secret_key
    aws_region: AWS region
    vpc_id:     AWS VPC id ( bubble id )
    igw_name:   Internet Gateway name
